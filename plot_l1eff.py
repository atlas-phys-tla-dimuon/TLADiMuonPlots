#%%
import coffea.processor
import coffea.hist
import matplotlib.pyplot as plt
import numpy as np
import awkward as ak
import pandas as pd

from matplotlib.colors import LogNorm

from kkcoffea import samples
from kkcoffea import processors

import kkplot

from tla import config
from tla import tools
from tla import l1

# %%
class L1Plots(processors.HistProcessor):
    def __init__(self, weight_column='',xsecdb=None, group=None):
        super().__init__(weight_column=weight_column, xsecdb=xsecdb, group=group)

        histtemplate=coffea.hist.Hist(
            "Cross-Section [pb]",
            coffea.hist.Cat("dataset", "Dataset"),
            coffea.hist.Bin("dr", "$\Delta{}R(\mu^{\mathrm{lead}},\mu^{\mathrm{subl}})$", 30,0,  6),
            coffea.hist.Bin("m" , "$m(\mu^{\mathrm{lead}},\mu^{\mathrm{subl}})$"        , 50,0,100),
            )
            
        self.accumulator += coffea.processor.dict_accumulator({
            "all" : histtemplate.copy(),
        })

        # Initialize selections
        self.selections=[]
        self.selections.append(l1.DiMuonSelection())
        self.selections.append(l1.DiMuonSelection(thr=8))
        self.selections.append(l1.DiMuonSelection(thr=3,m=(7,22),dr=(0,2)))
        for selection in self.selections:
            self.accumulator += coffea.processor.dict_accumulator({
                selection.name() : histtemplate.copy(),
            })

    def process(self, events):
        output=super().process(events)

        dataset = events.metadata['dataset']

        # Helpful variables
        events['mu_theta'] = 2*np.arctan(np.exp(-events['mu_eta']))
        events['mu_p' ]=events['mu_pt']/np.sin(events['mu_theta'])
        events['mu_pz']=events['mu_pt']/np.tan(events['mu_theta'])
        events['mu_px']=events['mu_pt']*np.cos(events['mu_phi'])
        events['mu_py']=events['mu_pt']*np.sin(events['mu_phi'])

        events['l1mu_theta'] = 2*np.arctan(np.exp(-events['l1mu_eta']))
        events['l1mu_p' ]=events['l1mu_thrValue']/np.sin(events['l1mu_theta'])
        events['l1mu_pz']=events['l1mu_thrValue']/np.tan(events['l1mu_theta'])
        events['l1mu_px']=events['l1mu_thrValue']*np.cos(events['l1mu_phi'])
        events['l1mu_py']=events['l1mu_thrValue']*np.sin(events['l1mu_phi'])

        #
        # Event selection

        # Di muon events (baseline)
        dimuon=events[ak.num(events['mu_pt'])>2]

        dimuon['p']=np.sqrt((dimuon['mu_px'][:,0]+dimuon['mu_px'][:,1])**2+(dimuon['mu_py'][:,0]+dimuon['mu_py'][:,1])**2+(dimuon['mu_pz'][:,0]+dimuon['mu_pz'][:,1])**2)
        dimuon['e']=dimuon['mu_p'][:,0]+dimuon['mu_p'][:,1]
        dimuon['m']=np.sqrt(dimuon['e']**2-dimuon['p']**2)

        dimuon['dr']=tools.deltaR(dimuon['mu_eta'][:,0],dimuon['mu_phi'][:,0],dimuon['mu_eta'][:,1],dimuon['mu_phi'][:,1])

        output['all'].fill(
            dataset=dataset, dr=dimuon['dr'], m=dimuon['m']/1e3, weight=self.weights(dimuon)
            )

        # Dimuon events at L1
        dil1muon=dimuon[ak.num(dimuon['l1mu_thrValue'])>=2]

        dil1muon['l1_p']=np.sqrt((dil1muon['l1mu_px'][:,0]+dil1muon['l1mu_px'][:,1])**2+(dil1muon['l1mu_py'][:,0]+dil1muon['l1mu_py'][:,1])**2+(dil1muon['l1mu_pz'][:,0]+dil1muon['l1mu_pz'][:,1])**2)
        dil1muon['l1_e']=dil1muon['l1mu_p'][:,0]+dil1muon['l1mu_p'][:,1]
        dil1muon['l1_m']=np.sqrt(dil1muon['l1_e']**2-dil1muon['l1_p']**2)
        dil1muon['l1_dr']=tools.deltaR(dil1muon['l1mu_eta'][:,0],dil1muon['l1mu_phi'][:,0],
                                       dil1muon['l1mu_eta'][:,1],dil1muon['l1mu_phi'][:,1])


        # Loop over selections
        for selection in self.selections:
            mask=selection.mask(dil1muon)
            myevents=dil1muon[mask]

            output[selection.name()].fill(
                dataset=dataset, dr=myevents['dr'], m=myevents['m']/1e3, weight=self.weights(myevents)
                )

        return output

# %%
mysamples=samples.SampleManager('samples/background.yaml', datadir=config.datadir)
mysamples.add_sampledef('samples/signal.yaml')
fileset = mysamples.fileset()

if getattr(mysamples,'mc',False):
    xsecdb=pd.read_csv('PMGxsecDB_mc16.txt',sep='\s+',skiprows=1,names=['dataset_number','physics_short','crossSection','genFiltEff','kFactor','relUncertUP','relUncertDOWN','generator_name','etag'],index_col='dataset_number')
else:
    xsecdb=None

out = coffea.processor.run_uproot_job(
    fileset,
    treename="physics",
    processor_instance=L1Plots(weight_column=getattr(mysamples, 'weight_column', None),xsecdb=xsecdb,group=mysamples.group()),
    executor=coffea.processor.iterative_executor
)

# %%
for i in out['all'].identifiers('dataset'):
    myhall =out['all' ][i]

    fig, ax = plt.subplots(1,1)

    for histname in out:
        if not histname.startswith('2MU'):
            continue

        myh=out[histname][i]

        coffea.hist.plotratio(myh.sum('dataset','dr'),
                            myhall.sum('dataset','dr'),
                            unc='normal',label=histname,
                            error_opts={}, xerr=True,
                            ax=ax, clear=False)
    ax.legend()
    ax.set_title(i)
    kkplot.ticks(ax.xaxis, 10, 2)
    ax.set_ylim(0,1)
    kkplot.ticks(ax.yaxis, 0.1, 0.02)
    ax.set_ylabel('Trigger Efficiency')
    fig.tight_layout()
    fig.show()
    fig.savefig(f'{i}_eff.png')

# %% Make a plot of different selections, comparing against signal
hall=out["all"]

den=out['all'].sum('dr','m').values()

for histname in out:
    if not histname.startswith('2MU'):
        continue
    myh=out[histname]

    fig, ax = plt.subplots(1,1)

    # plot vs mass
    coffea.hist.plotratio(myh['DrellYan'].sum('dataset','dr'),
                        hall['DrellYan'].sum('dataset','dr'),
                        unc='normal',label="Drell Yan",
                        error_opts={}, xerr=True,
                        ax=ax, clear=False)

    # example plots
    num=myh.sum('dr','m').values()

    plt.plot(15,num['451011',]/den['451011',], '*', label='$m_R$ = 15 GeV')
    plt.plot(45,num['451010',]/den['451010',], '*', label='$m_R$ = 45 GeV')

    ax.legend()
    ax.set_title(histname)
    ax.set_ylim(0,1)
    ax.set_ylabel('Trigger Efficiency')
    fig.tight_layout()
    fig.show()
    fig.savefig(f'{histname}_eff.png')

# %%
