#%%
import coffea.processor
import coffea.hist
import matplotlib.pyplot as plt
import numpy as np
import awkward as ak
import pandas as pd

from kkhep import samples
from kkhep import processors

# %%
class MyProcessor(processors.HistProcessor):
    def __init__(self, xsecdb=None, group=None):
        super().__init__(xsecdb, group)

        self.accumulator += coffea.processor.dict_accumulator({
            "l1" : coffea.hist.Hist(
                "Raw Entries",
                coffea.hist.Cat("dataset", "Dataset"),
                coffea.hist.Bin("l1mu_dr", "$\DeltaR(\mu^{\text{L1,0}},\mu^{\text{L1,1}})", 100,0,100),
                ),
            "mass" : coffea.hist.Hist(
                "Cross-Section [pb/1 GeV]",
                coffea.hist.Cat("dataset", "Dataset"),
                coffea.hist.Bin("mass", "$m_{\mu\mu}$ [GeV]", 100,0,100)
                ),
            "dimuon_pt" : coffea.hist.Hist(
                "Raw Entries",
                coffea.hist.Cat("dataset", "Dataset"),
                coffea.hist.Bin("mu0_pt", "$p_{\mu,0}$ [GeV]", 100,0,100),
                coffea.hist.Bin("mu1_pt", "$p_{\mu,0}$ [GeV]", 100,0,100)
                ),
        })

    def process(self, events):
        output=super().process(events)

        dataset = events.metadata['dataset']

        # Event selection
        events['mu_theta'] = 2*np.arctan(np.exp(-events['mu_eta']))
        events['mu_p' ]=events['mu_pt']/np.sin(events['mu_theta'])
        events['mu_pz']=events['mu_pt']/np.tan(events['mu_theta'])
        events['mu_px']=events['mu_pt']*np.cos(events['mu_phi'])
        events['mu_py']=events['mu_pt']*np.sin(events['mu_phi'])

        dimuon=events[ak.num(events['mu_pt'])>=2]
        dimuon['p']=np.sqrt((dimuon['mu_px'][:,0]+dimuon['mu_px'][:,1])**2+(dimuon['mu_py'][:,0]+dimuon['mu_py'][:,1])**2+(dimuon['mu_pz'][:,0]+dimuon['mu_pz'][:,1])**2)
        dimuon['e']=dimuon['mu_p'][:,0]+dimuon['mu_p'][:,1]
        dimuon['m']=np.sqrt(dimuon['e']**2-dimuon['p']**2)

        # Fill histograms
        output['mass'].fill(
            dataset=dataset, mass=dimuon['m']/1e3, weight=dimuon['mcEventWeights'][:,0]
            )

        output['dimuon_pt'].fill(
            dataset=dataset, mu0_pt=dimuon['mu_pt'][:,0]/1e3, mu1_pt=dimuon['mu_pt'][:,1]/1e3
            )

        return output

# %%
mysamples=samples.SampleManager('samples.yaml')
fileset = mysamples.fileset()

xsecdb=pd.read_csv('PMGxsecDB_mc16.txt',sep='\s+',skiprows=1,names=['dataset_number','physics_short','crossSection','genFiltEff','kFactor','relUncertUP','relUncertDOWN','generator_name','etag'],index_col='dataset_number')

out = coffea.processor.run_uproot_job(
    fileset,
    treename="physics",
    processor_instance=MyProcessor(xsecdb,group=mysamples.group()),
    executor=coffea.processor.iterative_executor
)

# %%
coffea.hist.plot1d(out['mass'], overlay='dataset')
plt.ylim(0,300)
plt.show()
plt.savefig('mass.png')
plt.clf()
# %%
h=out['dimuon_pt']
for i in out['dimuon_pt'].identifiers('dataset'):
    h=out['dimuon_pt'][i]
    coffea.hist.plot2d(h.sum('dataset'), 'mu0_pt')
    plt.title(i)
# %%
