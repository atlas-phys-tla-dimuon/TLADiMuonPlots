#%%
import coffea.processor
import coffea.hist
import matplotlib.pyplot as plt
import numpy as np
import awkward as ak
import pandas as pd

from matplotlib.colors import LogNorm

from kkhep import samples
from kkhep import processors

# %%
class L1Plots(processors.HistProcessor):
    def __init__(self, xsecdb=None, group=None):
        super().__init__(xsecdb, group)

        self.accumulator += coffea.processor.dict_accumulator({
            "l1_dimuon" : coffea.hist.Hist(
                "Raw Entries",
                coffea.hist.Cat("dataset", "Dataset"),
                coffea.hist.Bin("dr", "$\Delta{}R(\mu^{\mathrm{L1,lead}},\mu^{\mathrm{L1,subl}})$", 30,0, 6),
                coffea.hist.Bin("m" , "$m(\mu^{\mathrm{L1,lead}},\mu^{\mathrm{L1,subl}})$"        , 75,0,75),
                ),
            "l1_eta" : coffea.hist.Hist(
                "Raw Entries",
                coffea.hist.Cat("dataset", "Dataset"),
                coffea.hist.Bin("source", "Source", 4,-0.5,3.5),
                coffea.hist.Bin("eta"   , "$\eta^{\mathrm{L1 }\mu}$"        , 100,-3,3),
                ),
        })

    def process(self, events):
        output=super().process(events)

        dataset = events.metadata['dataset']

        # Helpful variables
        events['l1mu_theta'] = 2*np.arctan(np.exp(-events['l1mu_eta']))
        events['l1mu_p' ]=events['l1mu_thrValue']/np.sin(events['l1mu_theta'])
        events['l1mu_pz']=events['l1mu_thrValue']/np.tan(events['l1mu_theta'])
        events['l1mu_px']=events['l1mu_thrValue']*np.cos(events['l1mu_phi'])
        events['l1mu_py']=events['l1mu_thrValue']*np.sin(events['l1mu_phi'])

        dimuon=events[ak.num(events['l1mu_thrValue'])>=2]

        # Event selection
        dimuon=events[ak.num(events['l1mu_thrNumber'])>=2]

        dimuon['l1p']=np.sqrt((dimuon['l1mu_px'][:,0]+dimuon['l1mu_px'][:,1])**2+(dimuon['l1mu_py'][:,0]+dimuon['l1mu_py'][:,1])**2+(dimuon['l1mu_pz'][:,0]+dimuon['l1mu_pz'][:,1])**2)
        dimuon['l1e']=dimuon['l1mu_p'][:,0]+dimuon['l1mu_p'][:,1]
        dimuon['l1m']=np.sqrt(dimuon['l1e']**2-dimuon['l1p']**2)

        dimuon['l1dr']=np.sqrt((dimuon['l1mu_eta'][:,0]-dimuon['l1mu_eta'][:,1])**2+(dimuon['l1mu_phi'][:,0]-dimuon['l1mu_phi'][:,1])**2)

        # Fill histograms
        output['l1_dimuon'].fill(
            dataset=dataset, dr=dimuon['l1dr'], m=dimuon['l1m']/1e3, weight=dimuon['EBWeight']
            )

        output['l1_eta'].fill(
            dataset=dataset, source=ak.flatten(events['l1mu_source']), eta=ak.flatten(events['l1mu_eta'])
            )

        return output

# %%
mysamples=samples.SampleManager('samples.yaml')
fileset = mysamples.fileset()

xsecdb=None #pd.read_csv('PMGxsecDB_mc16.txt',sep='\s+',skiprows=1,names=['dataset_number','physics_short','crossSection','genFiltEff','kFactor','relUncertUP','relUncertDOWN','generator_name','etag'],index_col='dataset_number')

out = coffea.processor.run_uproot_job(
    fileset,
    treename="physics",
    processor_instance=L1Plots(xsecdb,group=mysamples.group()),
    executor=coffea.processor.iterative_executor
)

# %%
coffea.hist.plot1d(out['l1_dimuon'].sum('m'), overlay='dataset')
plt.tight_layout()
plt.savefig('l1_dimuon_dr.png')
plt.clf()
# %%
coffea.hist.plot1d(out['l1_dimuon'].sum('dr'), overlay='dataset')
plt.tight_layout()
plt.savefig('l1_dimuon_m.png')
plt.show()
plt.clf()
# %%
for i in out['l1_dimuon'].identifiers('dataset'):
    h=out['l1_dimuon'][i]
    coffea.hist.plot2d(h.sum('dataset'), 'm', patch_opts={'norm':LogNorm()})
    plt.title(i)
    plt.savefig(f'l1_dimuon_drvsm_{i}.png')
    plt.show()
plt.clf()
# %%
for i in out['l1_eta'].identifiers('dataset'):
    h=out['l1_eta'][i]
    coffea.hist.plot1d(h.sum('dataset'), overlay='source')
    plt.plot([ 1.05, 1.05],[0,1000],'k--')
    plt.plot([-1.05,-1.05],[0,1000],'k--')
    plt.title(i)
    plt.savefig(f'l1_eta_{i}.png')
    plt.show()
    plt.clf()

# %%
