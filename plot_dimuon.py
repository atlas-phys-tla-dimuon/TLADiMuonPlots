#%%
import coffea.processor
import coffea.hist
import matplotlib.pyplot as plt
import numpy as np
import awkward as ak
import pandas as pd

from matplotlib.colors import LogNorm

from kkcoffea import samples
from kkcoffea import processors

from tla import muons

from tla import config

# %%
class DimuonPlots(processors.HistProcessor):
    def __init__(self, weight_column='',xsecdb=None, group=None):
        super().__init__(weight_column=weight_column, xsecdb=xsecdb, group=group)

        self.accumulator += coffea.processor.dict_accumulator({
            "dimuon" : coffea.hist.Hist(
                "",
                coffea.hist.Cat("dataset", "Dataset"),
                coffea.hist.Bin("dr", "$\Delta{}R(\mu^{\mathrm{lead}},\mu^{\mathrm{subl}})$", 30,0, 6),
                coffea.hist.Bin("m" , "$m(\mu^{\mathrm{lead}},\mu^{\mathrm{subl}})$"        , 100,0,100),
                ),
        })

    def process(self, events):
        output=super().process(events)

        dataset = events.metadata['dataset']

        # List of particle branches
        br_l1mu=[key for key in dir(events) if key.startswith('l1mu_')]
        br_mu  =[key for key in dir(events) if key.startswith('mu_'  )]

        # Sort the muons
        muptsort=ak.argsort(events['mu_pt'], ascending=False)
        for br in br_mu:
            events[br]=events[br][muptsort]

        # Helpful variables
        events['mu_theta'] = 2*np.arctan(np.exp(-events['mu_eta']))
        events['mu_p' ]=events['mu_pt']/np.sin(events['mu_theta'])
        events['mu_pz']=events['mu_pt']/np.tan(events['mu_theta'])
        events['mu_px']=events['mu_pt']*np.cos(events['mu_phi'])
        events['mu_py']=events['mu_pt']*np.sin(events['mu_phi'])

        # Event selection
        dimuon=events[ak.num(events['mu_pt'])>=2]

        dimuon['p']=np.sqrt((dimuon['mu_px'][:,0]+dimuon['mu_px'][:,1])**2+(dimuon['mu_py'][:,0]+dimuon['mu_py'][:,1])**2+(dimuon['mu_pz'][:,0]+dimuon['mu_pz'][:,1])**2)
        dimuon['e']=dimuon['mu_p'][:,0]+dimuon['mu_p'][:,1]
        dimuon['m']=np.sqrt(dimuon['e']**2-dimuon['p']**2)

        dimuon['dr']=np.sqrt((dimuon['mu_eta'][:,0]-dimuon['mu_eta'][:,1])**2+(dimuon['mu_phi'][:,0]-dimuon['mu_phi'][:,1])**2)

        # Fill histograms
        output['dimuon'].fill(
            dataset=dataset, dr=dimuon['dr'], m=dimuon['m']/1e3, weight=self.weights(dimuon)
            )

        return output

# %%
mysamples=samples.SampleManager('samples/EnhancedBias.yaml', datadir=config.datadir)
fileset = mysamples.fileset()

if getattr(mysamples,'mc',False):
    xsecdb=pd.read_csv('PMGxsecDB_mc16.txt',sep='\s+',skiprows=1,names=['dataset_number','physics_short','crossSection','genFiltEff','kFactor','relUncertUP','relUncertDOWN','generator_name','etag'],index_col='dataset_number')
else:
    xsecdb=None

out = coffea.processor.run_uproot_job(
    fileset,
    treename="physics",
    processor_instance=DimuonPlots(weight_column=getattr(mysamples, 'weight_column', None),xsecdb=xsecdb,group=mysamples.group()),
    executor=coffea.processor.iterative_executor
)

# %%
coffea.hist.plot1d(out['dimuon'].sum('m'), overlay='dataset')
plt.tight_layout()
plt.savefig('dimuon_dr.png')
plt.show()
plt.clf()
# %%
coffea.hist.plot1d(out['dimuon'].sum('dr'), overlay='dataset')
plt.yscale('log')
plt.ylim(1e1,1e4)
plt.tight_layout()
plt.savefig('dimuon_m.png')
plt.show()
plt.clf()

# %%
