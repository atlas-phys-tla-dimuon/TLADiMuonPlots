#!/usr/bin/env python

import setuptools

setuptools.setup(
    name='TLADiMuonPlots',
    version='0.0.1',
    description='TLA DiMuon Plotting scripts.',
    install_requires=[
        'coffea',
        'pyyaml'
      ]
)
