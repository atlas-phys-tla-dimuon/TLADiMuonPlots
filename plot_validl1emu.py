#%%
import coffea.processor
import coffea.hist
import matplotlib.pyplot as plt
import numpy as np
import awkward as ak
import pandas as pd

from kkcoffea import samples
from kkcoffea import processors

import kkplot

from tla import config
from tla import tools
from tla import l1
from tla import dimuon

# %%
def check_emu(events, name, emu):
    emu=emu.mask(events)
    sim=events[f'trigPassed_{name}']
    wrong=emu!=sim

    print('------------------------------------------------------')
    print(f'Wrong {name}:',np.sum(wrong),'/',len(events))

    # Print example wrong event
    ewrong=events[wrong] # Example event
    for i, e in enumerate(ewrong[:1]):
        print(f'=== {i}')
        print('Event Number:',e['eventNumber'])
        print('My Decision:',emu[wrong][i])
        print('Their Decision:',e[f'trigPassed_{name}'])
        print('thrNumber:', e['l1mu_thrNumber'])
        print('thrValue:', e['l1mu_thrValue'])
        print('eta:', e['l1mu_eta'])
        print('m:', e['l1_m'])
        print('dR:', e['l1_dr'])

class L1EmuValid(coffea.processor.ProcessorABC):
    def __init__(self):
        self.accumulator = coffea.processor.dict_accumulator()

        # Triggers to emulate
        self.L1_2MU3V = l1.DiMuonSelection(thr=3)
        self.L1_2MU5VF = l1.DiMuonSelection(thr=5)
        self.L1_BPH_2M9_0DR15_2MU3V = l1.DiMuonSelection(thr=3, m=(2,9), dr=(0,1.5))

    def process(self, events):
        output = self.accumulator.identity()

        # Prepare L1 muon inputs
        l1.decorate_l1topo_muons(events)
        
        # Check triggers
        check_emu(events, 'L1_2MU3V', self.L1_2MU3V)
        check_emu(events, 'L1_2MU5VF', self.L1_2MU5VF)
        check_emu(events, 'L1_BPH_2M9_0DR15_2MU3V', self.L1_BPH_2M9_0DR15_2MU3V)

        return output

    def postprocess(self, accumulator):
        return accumulator

# %%
mysamples=samples.SampleManager('samples/background.yaml', datadir=config.datadir)
fileset = mysamples.fileset()

if getattr(mysamples,'mc',False):
    xsecdb=pd.read_csv('PMGxsecDB_mc16.txt',sep='\s+',skiprows=1,names=['dataset_number','physics_short','crossSection','genFiltEff','kFactor','relUncertUP','relUncertDOWN','generator_name','etag'],index_col='dataset_number')
else:
    xsecdb=None

coffea.processor.run_uproot_job(
    fileset,
    treename="physics",
    processor_instance=L1EmuValid(),
    executor=coffea.processor.iterative_executor
)

# %%
