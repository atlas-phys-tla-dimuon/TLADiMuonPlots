import numpy as np
import awkward as ak

from tla import tools

def system_variables(events,prefix='mu'):
    """ Calculate the dimuon system kinematics. """
    p =np.sqrt((events[f'{prefix}_px'][:,0]+events[f'{prefix}_px'][:,1])**2+
               (events[f'{prefix}_py'][:,0]+events[f'{prefix}_py'][:,1])**2+
               (events[f'{prefix}_pz'][:,0]+events[f'{prefix}_pz'][:,1])**2)
    e =events[f'{prefix}_p'][:,0]+events[f'{prefix}_p'][:,1]
    m =np.sqrt(e**2-p**2)

    dr=tools.deltaR(events[f'{prefix}_eta'][:,0], events[f'{prefix}_phi'][:,0],
                    events[f'{prefix}_eta'][:,1], events[f'{prefix}_phi'][:,1])

    return p,e,m,dr

def allsystem_variables(events,prefix='mu'):
    """ Calculate dimuon system variables for all possible pairings. """
    ma,mb=ak.unzip(ak.argcombinations(events[f'{prefix}_pt'], 2))

    # Mass
    p=np.sqrt((events[f'{prefix}_px'][ma]+events[f'{prefix}_px'][mb])**2+
              (events[f'{prefix}_py'][ma]+events[f'{prefix}_py'][mb])**2+
              (events[f'{prefix}_pz'][ma]+events[f'{prefix}_pz'][mb])**2)
    e=events[f'{prefix}_p'][ma]+events[f'{prefix}_p'][mb]
    m=np.sqrt(e**2-p**2)

    # Angle differences
    dotAB=events[f'{prefix}_px'][ma]*events[f'{prefix}_px'][mb]+events[f'{prefix}_py'][ma]*events[f'{prefix}_py'][mb]
    AB=events[f'{prefix}_pt'][ma]*events[f'{prefix}_pt'][mb]
    dPhi=np.arccos(dotAB/AB)
    dEta=np.abs(events[f'{prefix}_eta'][ma]-events[f'{prefix}_eta'][mb])
    dR=np.sqrt(dPhi**2+dEta**2)

    return m, dPhi, dEta, dR