import numpy as np

def columns(events, prefix):
    """
    Return list of field names for a particle (aka ones starting with `prefix_`)
    """
    return [key for key in events.fields if key.startswith(f'{prefix}_')]

def mom_from_petaphi(events, prefix):
    """
    Calculate kinematic variables from p, eta, phi.
    """
    theta = 2*np.arctan(np.exp(-events[f'{prefix}_eta']))

    p =events[f'{prefix}_pt']/np.sin(theta)
    pz=events[f'{prefix}_pt']/np.tan(theta)
    px=events[f'{prefix}_pt']*np.cos(events[f'{prefix}_phi'])
    py=events[f'{prefix}_pt']*np.sin(events[f'{prefix}_phi'])

    return p,px,py,pz

def deltaPhi(phi0,phi1):
    """
    Calculate `dPhi` between two particles, taking wrap-around into account.
    """
    dphi=np.abs(phi0-phi1)
    dphi=np.where(dphi>np.pi,2*np.pi-dphi,dphi)
    return dphi


def deltaR(eta0,phi0,eta1,phi1):
    """
    Calculate `dR=sqrt(dPhi**2+dEta**2)` between two particles.
    """
    dphi=np.abs(phi0-phi1)
    dphi=np.where(dphi>np.pi,2*np.pi-dphi,dphi)
    return np.sqrt((eta0-eta1)**2+dphi**2)
