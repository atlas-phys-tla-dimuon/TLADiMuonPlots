"""
Helpful functions for dealing with L1 muons.
"""

import awkward as ak

from . import tools
from . import dimuon

RUN3_CAND_VETO_SHIFT= 16
CAND_VETO_SHIFT     = 28

def isVetoed(roiWord):
    """
    When the overlap handling is activated in the MuCTPI, candidates can be
    ignored in the multiplicity sum sent to the CTP. This flag tells you
    whether this particular candidate was ignored in the multiplicity sum.
    """
    return ((roiWord>>RUN3_CAND_VETO_SHIFT)&1)==1

def decorate_l1topo_muons(events):
    """
    Common actions to add useful variables, perform overlap removal and
    calculate L1Topo variables for muons stored in `events`.

    Modifications for L1 muons are done in-place.
    """
    # Overlap removal
    l1mu=tools.columns(events,'l1mu')

    veto=isVetoed(events['l1mu_roiWord'])
    for col in l1mu:
        events[col]=events[col][~veto]

    # Kinematic calculations
    events['l1mu_pt']=events['l1mu_thrValue'] # rename for convenience
    events['l1mu_p'] ,events['l1mu_px'] ,events['l1mu_py'] ,events['l1mu_pz'] = \
        tools.mom_from_petaphi(events, prefix='l1mu')

    # Potential rounding
    # https://gitlab.cern.ch/atlas/athena/-/blob/231dfa4e0d1e4728df84fd56bb55feff81fee027/Trigger/TrigT1/L1Topo/L1TopoSimulation/src/MuonInputProvider.cxx#L209
    #events['l1mu_pt' ]=ak.values_astype(events['l1mu_pt' ]*10, int)/10
    #events['l1mu_eta']=ak.values_astype(events['l1mu_eta']*10, int)/10
    #events['l1mu_phi']=ak.values_astype(events['l1mu_phi']*10, int)/10

    # L1Topo variables
    events['l1_m'], events['l1_dphi'], events['l1_deta'], events['l1_dr'] = \
        dimuon.allsystem_variables(events, 'l1mu')

class DiMuonSelection:
    """
    Build L1 selections for dimuon events.
    """
    def __init__(self, thr=None, m=None, dr=None):
        self.thr=thr
        self.m=m
        self.dr=dr

    def name(self):
        """
        Build L1 trigger name corresponding to the selection.
        """
        name='2MU'

        if self.thr is not None:
            name+=str(self.thr)

        if self.m is not None:
            name=f'{name}_{self.m[0]}M{self.m[1]}'

        if self.dr is not None:
            name=f'{name}_{self.dr[0]}DR{self.dr[1]}'

        return name

    def mask(self,events):
        """
        Build a selection mask for `events`.

        The `events` is epected to have the following variables declared:
        - `l1mu_*` for muon kinmetics
        - `l1_m`: invariant mass of all dimuon systems (calculated at L1)
        - `l1_dr`: dR between the all muonss (calculated at L1)
        """
        # Always require at least two muons
        mask=ak.num(events['l1mu_thrValue'])>=2
        events=events.mask[mask]

        if self.thr is not None:
            nmu=events['l1mu_thrValue']>=self.thr*1e3
            mask=ak.count_nonzero(nmu,axis=-1)>=2
            events=events.mask[mask]

        windows=ak.full_like(events['l1_m'], True, dtype=bool)

        if self.m is not None:
            l1_m=events['l1_m']/1e3
            mwin=(self.m[0]<=l1_m)&(l1_m<self.m[1])
            windows=windows&mwin

        if self.dr is not None:
            l1_dr=events['l1_dr']
            drwin=(self.dr[0]<=l1_dr)&(l1_dr<self.dr[1])
            windows=windows&drwin

        mask=ak.count_nonzero(windows,axis=-1)>0
        events=events.mask[mask]

        return ~ak.is_none(events)
